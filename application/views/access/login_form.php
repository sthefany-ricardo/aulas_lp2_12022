<div class="d-flex justify-content-center align-items-center" style="height: 100vh;">
  <div class="text-center">
    <h3 class="mb-5">Controle Financeiro Pessoal</h3>

      <form method="POST">
        <h2 class="text-black-100 fw-bold mb-2 text-uppercase">Login</h2>
        <p class="text-black-50 mb-5">Por favor, digite seu login e senha!</p>
        
        <div class="form-outline mb-4">
            <input type="email" id="email" name="email" class="form-control" />
            <label class="form-label" for="form1Example1">E-mail</label>
        </div>

        <div class="form-outline mb-4">
            <input type="password" id="senha" name="senha" class="form-control" />
            <label class="form-label" for="form1Example2">Senha</label>
        </div>
        
        <div class="row mb-4">
            <div class="col d-flex justify-content-center">
       
            <div class="form-check">
                <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="form1Example3"
                checked
                />
                <label class="form-check-label" for="form1Example3"> Manter conectado </label>
            </div>
            </div>

            <div class="col">
            
            <a href="#!">Esqueceu a senha?</a>
            </div>
        </div>
       
        <button type="submit" class="btn btn-primary btn-block">Entrar</button>
        
        <p class="red-text"> <?= $error ? 'Dados de acesso incorretos.' : '' ?> </p>
      </form>  
  </div>
</div>