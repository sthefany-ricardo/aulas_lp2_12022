Esta aplicação esta sendo desenvolvida durante as aulas de Linguagem de Programação II, disciplina do curso de Análise e Desenvolvimento de Sistemas do IFSP Campus Guarulhos.

---

## Ferramenta e linguagens

As linguaguens utilizadas para o desenvolvimento do app são:

- **CodeIgniter Framework**
- **MDBootstrap**
- **Banco da Dados MySQL**

As ferramentas utilizadas no desenvolvimento do app são:

- **XAMPP**
- **Visual Studio Code**
- **Git** (para controle de versão)
- **Bitbucket**
- **ScreenToGif** (para fazer os gifs de demonstração)

---

<!-- ### Logo do app iFinanças

**Logo iFinanças**

![Logo iFinanças](/src/assets/icon/Logo%20iFinan%C3%A7as.png)

**Logo iFinanças com fundo transparente**

![Logo iFinanças fundo transparente](/src/assets/icon/Logo%20iFinan%C3%A7as%20transparente.png)  -->

---
